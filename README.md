Paint Shop

Building the project:

Run command: 

> gradlew build



Project Structure

The project contains 3 modules: business, console-app  and rest



Business 

This module contains domain model classes and classes implementing algorinthms to solve the problem. There are two different methods of the problem solving provided: 
  ShanghaiPaintFactory provides a solution based on recursive calls
  BangkokPaintFactory  provides a solution based on the simplex method optimization. This class using as default because it is a classical solution for this kind of tasks
  
Please note there is no secret meaning in the names of the classes - it's just the names of towns and nothing more


console-app

This module wraps the functionality implemented in module business in coommand line utility. The utility can be running in interactive and non-interactive mode

In interactive mode run application with command:

>java -jar console-app/build/libs/console-app.jar

The application will stared with command promt. Then you can type the commad:

> process path/to/your/file.txt

or 

> process path/to/your/file.txt --o path/to/your/ouutput/fil.tst

In the first case the processing result will be printed on screen, oth the second one the output will be duplicated in output file 

for non-interactive mode just run the command:

> java -jar console-app/build/libs/console-app.jar process path/to/your/file.txt

or 

> java -jar console-app/build/libs/console-app.jar process path/to/your/file.txt --o path/to/your/ouutput/fil.tst

The application will be stopped after work finished


Rest

This module wraps the functionality implemented in module business in RESTFuul service

To run the service exacute command:

> java -jar rest/build/libs/paintshop-svc-0.0.1.jar

The service will be availiabe for POST requests by the address: http://localhost:8080/process

It accepts a single string parameter in request body which should looks like e,g: "2 \n5\n3\n1 1 1\n2 1 0 2 01 5 0\n1\n2\n1 1 0\n1 1 1"


AWS Elastic Beanstalk deployment package

when project built, the AWS EB package can be found in folder rest\build\distributions - you can delpoy it on the  AWS Elastic Beanstalk as is using the AWS Console (requred to configuring the Security Groups). After deployment the service will be available by URL htttp://you.public.dns.amazonaws.com/process