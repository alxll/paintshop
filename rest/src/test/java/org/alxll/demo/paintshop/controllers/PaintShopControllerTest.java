package org.alxll.demo.paintshop.controllers;

import org.alxll.demo.paintshop.alg.PaintFactory;
import org.alxll.demo.paintshop.domain.ColorOption;
import org.alxll.demo.paintshop.domain.Paint;
import org.alxll.demo.paintshop.domain.Survey;
import org.alxll.demo.paintshop.parser.PaintShopReaderFactory;
import org.alxll.demo.paintshop.parser.PaintShopReaderScannerImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.Reader;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@RunWith(SpringJUnit4ClassRunner.class)
@RunWith(MockitoJUnitRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class PaintShopControllerTest {
    @Mock
    private PaintFactory paintFactory;


    @Mock
    private PaintShopReaderFactory paintShopReaderFactory;

    @Autowired
    @InjectMocks
    PaintShopController paintShopController;

    private MockMvc rest;


    @Before
    public void setUp() throws Exception {
        when(paintFactory.buildPaintRange(any(Survey.class))).thenReturn(
                Stream.of(new Paint(1, ColorOption.matte), new Paint(2, ColorOption.matte), new Paint(3, ColorOption.glossy)).collect(Collectors.toCollection(() ->
                        new TreeSet<Paint>((p1, p2) -> Integer.compare(p1.colorIndex(), p2.colorIndex()))
                ))
        );

        //Alxll: The Reader mock must be provided! Just forgot about it :(
        when(paintShopReaderFactory.paintShopReader(any(Reader.class))).thenAnswer(
                invocation -> {
                    Object[] args = invocation.getArguments();
                    return new PaintShopReaderScannerImpl((Reader)args[0]);
                });

        rest = MockMvcBuilders.standaloneSetup(paintShopController).build();
    }

    @Test
    public void processSurveys() throws Exception {
        //Any string can be sent
        String input = new StringBuilder()
                .append("2").append("\n")

                .append("5").append("\n")
                .append("3").append("\n")
                .append("1 1 1").append("\n")
                .append("2 1 0 2 0").append("\n")
                .append("1 5 0").append("\n")

                .append("1").append("\n")
                .append("2").append("\n")
                .append("1 1 0").append("\n")
                .append("1 1 1").append("\n")

                .toString();

        rest.perform(post("/process")
                .content(input)
                .contentType(MediaType.TEXT_PLAIN)
                .accept(MediaType.TEXT_PLAIN))

                .andExpect(status().isOk())
                .andExpect(content().string(containsString("1 1 0")));

    }

    @Test
    public void processSurveysInvalidMethod() throws Exception {
        //Any string can be sent
        String input = new StringBuilder()
                .append("2").append("\n")

                .append("5").append("\n")
                .append("3").append("\n")
                .append("1 1 1").append("\n")
                .append("2 1 0 2 0").append("\n")
                .append("1 5 0").append("\n")

                .append("1").append("\n")
                .append("2").append("\n")
                .append("1 1 0").append("\n")
                .append("1 1 1").append("\n")

                .toString();

        rest.perform(put("/process")
                .content(input)
                .contentType(MediaType.TEXT_PLAIN)
                .accept(MediaType.TEXT_PLAIN))

                .andExpect(status().is4xxClientError());

    }


    @Test
    public void test404() throws Exception {

        rest.perform(post("/invalid")
                .contentType(MediaType.TEXT_PLAIN)
                .accept(MediaType.TEXT_PLAIN))

                .andExpect(status().isNotFound());
    }


}