package org.alxll.demo.paintshop.controllers;

import org.alxll.demo.paintshop.alg.PaintFactory;
import org.alxll.demo.paintshop.exceptions.PaintShopServiceException;
import org.alxll.demo.paintshop.parser.PaintShopReader;
import org.alxll.demo.paintshop.parser.PaintShopReaderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * RESTFul service wrapping the Paint Shop functionality
 */
@RestController
public class PaintShopController {
    @Autowired
    private PaintShopReaderFactory paintShopReaderFactory;


    @Autowired
    private PaintFactory paintFactory;


    /**
     * Provides REST interface for root URL. Requires for testing only
     * @return
     */
    @RequestMapping(value = "", method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT}, consumes = MediaType.ALL_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String root(){
        return "Ok";
    }

    /**
     * REST service provides the input data processing in accordance to the project requirements
     * @param input data in format defined by the project requirements. e,g: "2 \n5\n3\n1 1 1\n2 1 0 2 01 5 0\n1\n2\n1 1 0\n1 1 1"
     * @return result of the processing e.g: "Case #1: 1 0 0 0 0\nCase #2: IMPOSSIBLE"
     */
    @RequestMapping(value = "process", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public String processSurveys(@RequestBody String input){
        StringReader reader = new StringReader(input);



        try(PaintShopReader r = paintShopReaderFactory.paintShopReader(new StringReader(input))){
            AtomicInteger indx = new AtomicInteger(1);
            List<String> result = r.read()
                    .map(paintFactory::buildPaintRange)
                    .map(paints -> paints.stream()
                            .map(p -> new StringBuilder().append(p.colorOption().ordinal()))
                            .collect(Collectors.reducing((s1, s2) -> s1.append(" ").append(s2)))
                            .orElse(new StringBuilder("IMPOSSIBLE"))
                            .toString())
                    .map(cs -> "Case #"+indx.getAndIncrement()+": "+cs)
                    .collect(Collectors.toList());

            return result.stream().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new PaintShopServiceException(e);
        }
    }
}

