package org.alxll.demo.paintshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
/**
 * Main class to start RESTFul service
 */
public class PaintShopSvc {
    public static void main(String[] args) {
        SpringApplication.run(PaintShopSvc.class, args);
    }
}
