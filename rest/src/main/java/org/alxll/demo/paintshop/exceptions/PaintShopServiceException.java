package org.alxll.demo.paintshop.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Service Error")
public class PaintShopServiceException extends RuntimeException{

    public PaintShopServiceException(){
        super();
    }

    public PaintShopServiceException(String msg){
        super(msg);
    }

    public PaintShopServiceException(Throwable t){
        super(t);
    }

    public PaintShopServiceException(String msg, Throwable t){
        super(msg, t);
    }
}
