package org.alxll.demo.paintshop.cfg;

import org.alxll.demo.paintshop.PaintShop;
import org.alxll.demo.paintshop.alg.PaintFactory;
import org.alxll.demo.paintshop.parser.PaintShopReaderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring configuration
 */
@Configuration
public class PaintShopRestConfig {

    @Bean
    public PaintShop paintShop(){
        return new PaintShop();
    }

    @Bean
    public PaintFactory paintFactory(){
        return paintShop().defaultPaintFactory();
    }

    @Bean(name="paintShopReaderFactory")
    public PaintShopReaderFactory paintShopReaderFactory() {
        return new PaintShopReaderFactory();
    }
}
