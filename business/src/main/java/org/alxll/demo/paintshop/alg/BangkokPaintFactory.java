package org.alxll.demo.paintshop.alg;


import org.alxll.demo.paintshop.domain.ColorOption;
import org.alxll.demo.paintshop.domain.Customer;
import org.alxll.demo.paintshop.domain.Paint;
import org.alxll.demo.paintshop.domain.Survey;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.linear.*;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * PaintFactory implementation based on the simplex method of solving linear programming problem (sorry for literal translation from Russian ;( )
 * @see <a href="https://en.wikipedia.org/wiki/Simplex_algorithm">Wikipedia: Simplex algorithm</a>
 */
public class BangkokPaintFactory implements PaintFactory{

    //Something wrong with with algorithm
    private class InvalidSolutionException extends RuntimeException{
        public InvalidSolutionException(){
            super();
        }
    }

    //Helper class wrapping the simplex solver functionality
    private class PaintFactorySolver{
        private Survey survey;

        PaintFactorySolver(Survey survey){
            this.survey = survey;
        }

        private int varNumber(){
            return survey.colorsTotal()* ColorOption.values().length;
        }

        private double[] zeroCoefficientArray(){
            return new double[varNumber()];
        }

        // Objective function coefficients
        // summarizes the total cost of all colors - matte cost 0, matte - 1
        private double[] costFunctionCoefficientArray(){
            return IntStream.range(0,survey.colorsTotal())
                    .mapToObj(i -> Arrays.stream(ColorOption.values()).map(c -> new Paint(1, c)))
                    .flatMap(Function.identity())
                    .map(p -> p.colorOption().ordinal())
                    .mapToDouble(Integer::doubleValue)
                    .toArray();
        }

        // "customer constraints" function coefficients
        // describes constraint sounds like "at least one color from customer preferences list has to be present"
        private double[] customerConstraintCoefficientArray(Customer c){
            double[] result = zeroCoefficientArray();

            c.paints().stream().forEach(p ->{
                int opt = p.colorOption().ordinal();
                int indx =  (p.colorIndex()-1)*ColorOption.size()+opt;
                result[indx] = 1.0;
            });
            return result;
        }

        // "color constraint" function coefficient
        // describes constraint which sounds like "the only one color option (glossy or mate) may present"
        private double[] colorConstraintCoefficientArray(int colorIndex){
            double[] result = zeroCoefficientArray();
            Arrays.stream(ColorOption.values()).forEach(c ->
                    result[colorIndex*ColorOption.size()+c.ordinal()] = 1.0
            );

            return result;
        }

        public SortedSet<Paint> solve(){
            LinearObjectiveFunction costFunction = new LinearObjectiveFunction(costFunctionCoefficientArray(), 0);

            List<LinearConstraint> customerConstraints = survey.customers().stream()
                    .map(this::customerConstraintCoefficientArray)
                    .map(arr -> new LinearConstraint(arr, Relationship.GEQ, 1))
                    .collect(Collectors.toList());

            List<LinearConstraint> colorConstraints = IntStream.range(0, survey.colorsTotal())
                    .mapToObj(this::colorConstraintCoefficientArray)
                    .map(arr -> new LinearConstraint(arr, Relationship.EQ, 1))
                    .collect(Collectors.toList());

            SimplexSolver simplexSolver = new SimplexSolver();

            List<LinearConstraint> constraints = new ArrayList<LinearConstraint>(){{
                addAll(customerConstraints);
                addAll(colorConstraints);
            }};

            try {
                PointValuePair solution = simplexSolver.optimize(costFunction, new LinearConstraintSet(constraints), GoalType.MINIMIZE, new NonNegativeConstraint(true));

                int[] solutionCoefficients = Arrays.stream(solution.getPoint()).mapToInt(d -> Double.valueOf(d).intValue()).toArray();
                int n = ColorOption.values().length;
                AtomicInteger indx = new AtomicInteger();



                SortedSet<Paint> paintsFound = IntStream.range(0, survey.colorsTotal())
                        .mapToObj(i -> Arrays.copyOfRange(solutionCoefficients, i * n, i * n + n))
                        .map(arr -> {
                            int i = IntStream.range(0, arr.length).filter(ii -> arr[ii]>0).findFirst().orElse(-1);
                            if(i<0){
                                throw new InvalidSolutionException();
                            }
                            return ColorOption.valueOf(i);
                        })
                        .map(o -> new Paint(indx.incrementAndGet(), o))
                        .collect(Collectors.toCollection(() ->
                                new TreeSet<Paint>((p1, p2) -> Integer.compare(p1.colorIndex(), p2.colorIndex()))
                        ));

                return paintsFound;
            }
            catch (NoFeasibleSolutionException | InvalidSolutionException e){
                return Collections.emptySortedSet();
            }
        }
    }


    /**
     * @see PaintFactory#buildPaintRange(Survey)
     */
    @Override
    public SortedSet<Paint> buildPaintRange(Survey survey) {

        PaintFactorySolver solver = new PaintFactorySolver(survey);
        return solver.solve();
    }
}
