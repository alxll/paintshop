package org.alxll.demo.paintshop.alg;


import org.alxll.demo.paintshop.domain.Paint;
import org.alxll.demo.paintshop.domain.Customer;
import org.alxll.demo.paintshop.domain.Survey;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * PaintFactory implementation based on recursion
 */
public class ShanghaiPaintFactory implements PaintFactory{

    // This is not actually required but it quite convenient for development (well... and for experiments...)
    // but in facts it is just a container for sorted paints from the customers' preferences
    private class SurveyEntry {
        private List<Paint> paints;

        SurveyEntry(Customer customer){
            paints = customer.paints().stream().sorted((p1, p2) -> p1.colorOption().compareTo(p2.colorOption())).collect(Collectors.toList());
        }

        public List<Paint> paints(){
            return  paints;
        }
    }

    // One step of the recursion. The head of the entries processed and the tail passed on the next step
    // entries MAUS be sorted by length (shortest on top)
    // The second param contains the paints collected on previous recursion steps
    private boolean offer(Queue<SurveyEntry> entries, Map<Integer, Paint> accum){
        if(entries.isEmpty()){
            return true;
        }

        SurveyEntry ps = entries.poll();

        if(ps.paints().stream().anyMatch(p -> accum.containsKey(p.colorIndex()) && accum.get(p.colorIndex()).equals(p))){
            return offer(entries, accum);
        }

        for(Paint p: ps.paints()){
            if(accum.putIfAbsent(p.colorIndex(), p)==null){
                if(offer(entries, accum)){
                    return true;
                }
                accum.remove(p.colorIndex());
            }
        }

        return false;
    }

    /**
     * @see PaintFactory#buildPaintRange(Survey)
     * @param testCase
     * @return
     */
    @Override
    public SortedSet<Paint> buildPaintRange(Survey testCase) {
        Map<Integer, Paint> result= new HashMap<>();

        Queue<SurveyEntry> surveyEntries = testCase.customers().stream()
                .sorted((s1, s2) -> Integer.compare(s1.size(), s2.size()))
                .map(SurveyEntry::new)
                .collect(Collectors.toCollection(LinkedList::new));


        offer(surveyEntries, result);

        return result.isEmpty()?
                Collections.emptySortedSet():
                IntStream.range(1, testCase.colorsTotal()+1)
                        .mapToObj(i -> result.getOrDefault(i, new Paint(i)))
                        .collect(Collectors.toCollection(() ->
                                new TreeSet<Paint>((p1, p2) -> Integer.compare(p1.colorIndex(), p2.colorIndex()))
                        ));
    }
}
