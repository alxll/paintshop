package org.alxll.demo.paintshop.parser;


import org.alxll.demo.paintshop.domain.ColorOption;
import org.alxll.demo.paintshop.domain.Customer;
import org.alxll.demo.paintshop.domain.Survey;

import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 *  Encapsulates parsing of the input data
 */
/*
    Alxll: This is not a good reader implementation - developed quickly but very poor control of the input source format
           It has to be replaced with reading input source into lines with checking each line using regex
*/
public class PaintShopReaderScannerImpl implements PaintShopReader {
    private final Scanner scanner;

    private boolean closed = false;


    /**
     * Constructs PaintShopReader from given Reader
     * @param r
     */
    public PaintShopReaderScannerImpl(Reader r){
        Objects.requireNonNull(r);
        scanner = new Scanner(r);
    }


    /**
     *
     * @return true if the reader open
     */
    @Override
    public boolean isOpen(){
        return !closed;
    }

    /**
     * Reads and parse the input source
     * @return Stream oof Survey
     */
    @Override
    public Stream<Survey> read(){
        int surveysNum = scanner.nextInt();
        Iterator<Survey> iter = surveyIterator(surveysNum, this::readSurvey);
        return StreamSupport.stream(Spliterators.spliterator(iter, surveysNum, Spliterator.ORDERED | Spliterator.NONNULL), false);

    }

    private Survey readSurvey(){
        if(!isOpen()){
            throw new IllegalStateException("PaintShopReader is closed!");
        }

        int maxColors = scanner.nextInt(); scanner.nextLine();
        int customersNum = scanner.nextInt(); scanner.nextLine();
        Survey survey = surveyBuilder().build(maxColors, customersNum);

        for(int n=0; n<customersNum; n++){
            int paintsNum = scanner.nextInt();
            Customer customer = customerBuilder().customer(maxColors);
            for(int p = 0; scanner.hasNext() && p < paintsNum; p++){
                customer.addPaint(scanner.nextInt(), ColorOption.valueOf(scanner.nextInt()));
            };

            if(customer.size() < paintsNum){
                throw new PaintShopReaderException("Expected "+paintsNum+" paints but " + customer.size()+" found");
            }

            survey.addCustomer(customer);

            if(scanner.hasNext())
                scanner.nextLine();
        }
        return survey;
    }


    @Override
    public void close() throws IOException {
        if(isOpen()){
            try {
                scanner.close();
            }
            finally {
                closed = false;
            }
        }
    }
}
