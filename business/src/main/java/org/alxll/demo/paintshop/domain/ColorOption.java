package org.alxll.demo.paintshop.domain;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 *  Describes available types of paints
 */
public enum ColorOption {
    glossy, matte;

    public static int size(){
        return ColorOption.values().length;
    }

    public static ColorOption valueOf(int ord){
        try{
            return ColorOption.values()[ord];
        }
        catch(IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("Invalid value of color option: "+ord+". The only available values are: " +
                    Arrays.stream(ColorOption.values())
                            .map(ColorOption::ordinal)
                            .map(Object::toString)
                            .collect(Collectors.joining(",")));
        }
    }
}
