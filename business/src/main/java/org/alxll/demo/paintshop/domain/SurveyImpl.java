package org.alxll.demo.paintshop.domain;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.*;

/**
 * @see org.alxll.demo.paintshop.domain.Survey
 */
public class SurveyImpl implements Survey {
    private final int colorsTotal;
    private final int maxCustomers;

    private final Set<Customer> customers = new HashSet<>();


    /**
     * Constructs new Customer object with given max number of color and max number of customers
     * @param colorsTotal
     * @param maxCustomers
     */
    public SurveyImpl(int colorsTotal, int maxCustomers){
        if(colorsTotal<=0){
            throw new IndexOutOfBoundsException("Number of colors  must be positive!");
        }

        if(maxCustomers<=0){
            throw new IndexOutOfBoundsException("Max customers number must be positive!");
        }

        this.colorsTotal = colorsTotal;
        this.maxCustomers = maxCustomers;
    }



    @Override
    public int customersNumber() {
        return customers.size();
    }

    @Override
    public int maxCustomers() {
        return maxCustomers;
    }

    @Override
    public int colorsTotal() {
        return colorsTotal;
    }

    @Override
    public void addCustomer(Customer s){
        Objects.requireNonNull(s);
        if(customers.size() == maxCustomers){
            throw new IndexOutOfBoundsException("TestCase max capacity exceeded: "+ maxCustomers);
        }
        customers.add(s);
    }

    @Override
    public Collection<Customer> customers(){
        return Collections.unmodifiableCollection(customers);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("colorsTotal", colorsTotal)
                .append("maxCustomers", maxCustomers)
                .append("customers", customers)
                .toString();
    }
}
