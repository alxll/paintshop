package org.alxll.demo.paintshop.parser;


import org.alxll.demo.paintshop.domain.ColorOption;
import org.alxll.demo.paintshop.domain.Customer;
import org.alxll.demo.paintshop.domain.Paint;
import org.alxll.demo.paintshop.domain.Survey;
import org.alxll.demo.paintshop.exceptions.PaintShopReaderInputFormatException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * PaintShopReader implementation based on reading lines one by one from BufferedReader
 * Provides better control on input source format.
 */
//Alxll: Well the input format control still not the best. It can be improver by using specific token classes and perhaps FSM
//       or even by developing ANTLR like parser
public class PaintShopBufferedReaderImpl implements PaintShopReader{
    private static Pattern line_single_digit_pattern = Pattern.compile("^\\s*(\\d+)\\s*$");

    private final BufferedReader reader;
    private boolean closed = false;


    /**
     * Constructs PaintShopReader from java.io.Reader
     * @param r
     */
    public PaintShopBufferedReaderImpl(Reader r){
        Objects.requireNonNull(r);
        if(r instanceof BufferedReader){
            reader = (BufferedReader)r;
        }
        else {
            reader = new BufferedReader(r);
        }
    }

    /**
     * @see PaintShopReader#isOpen()
     * @return
     */
    @Override
    public boolean isOpen() {
        return !closed;
    }

    private String nextNonEmptyLine() {
        String result = null;
        try {


            while ( (result = reader.readLine()) != null && result.trim().length() == 0) {
            }
        }
        catch(IOException e){
            throw new NoSuchElementException();
        }

        if(result==null || result.isEmpty()){
            throw new NoSuchElementException();
        }

        return result;
    }

    private int readInt(String s){
        //
        Matcher m =line_single_digit_pattern.matcher(s);
        if(!m.matches()){
            throw new PaintShopReaderInputFormatException("Invalid entry \""+s+"\". Expected single digit");
        }
        return Integer.parseInt(m.group(1));
    }


    private int readNumberOfCases(){
        return readInt(nextNonEmptyLine());

    }

    private int readNumberOfPaints(){
        return readInt(nextNonEmptyLine());
    }

    private int readNumberOfCustomers(){
        return readInt(nextNonEmptyLine());
    }

    private Customer readCustomer(){
        String strCustomer = nextNonEmptyLine();
        try {
            int[] customerEntry = Arrays.stream(strCustomer.split("\\s+")).mapToInt(Integer::parseInt).toArray();
            if(customerEntry.length==0){
                throw new PaintShopReaderInputFormatException("Invalid entry \""+strCustomer+"\". Expected space delimited int values");
            }
            int numberOfPaints = customerEntry[0];
            if(customerEntry.length!=2*numberOfPaints+1){
                throw new PaintShopReaderInputFormatException("Invalid entry \""+strCustomer+"\". Unexpected entry format");
            }

            Customer c = customerBuilder().customer(numberOfPaints);

                IntStream.range(0, numberOfPaints)
                        .mapToObj(i -> Arrays.copyOfRange(customerEntry, i * 2 + 1, i * 2 + 3))
                        .map(e -> new Paint(e[0], ColorOption.valueOf(e[1]))                        )
                        .forEach(c::addPaint);

            return c;
        }
        catch(NumberFormatException e){
            throw new PaintShopReaderInputFormatException("Invalid entry \""+strCustomer+"\". Expected space delimited int values");
        }
        catch(RuntimeException re){
            throw new PaintShopReaderInputFormatException("Invalid entry \""+strCustomer, re);
        }

    }

    private Survey readSurvey(){
        int numberOfPaints = readNumberOfPaints();
        int numberOfCustomers = readNumberOfCustomers();
        Survey s = surveyBuilder().build(numberOfPaints, numberOfCustomers);
        while(numberOfCustomers>0){
            Customer c = readCustomer();
            try {
                s.addCustomer(c);
            }
            catch(RuntimeException re){
                throw new PaintShopReaderInputFormatException("Input format error: ", re);
            }
            numberOfCustomers--;
        }

        return s;
    }

    /**
     * @see PaintShopReader#read()
     * @return
     */
    @Override
    public Stream<Survey> read() {
        int surveysNum = readNumberOfCases();
        Iterator<Survey> iter = surveyIterator(surveysNum, this::readSurvey);

        return StreamSupport.stream(Spliterators.spliterator(iter, surveysNum, Spliterator.ORDERED | Spliterator.NONNULL), false);
    }

    /**
     * @see PaintShopReader#close()
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        if(isOpen()){
            try {
                reader.close();
            }
            finally {
                closed = false;
            }
        }
    }


}
