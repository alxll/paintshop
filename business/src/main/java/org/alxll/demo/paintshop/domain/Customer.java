package org.alxll.demo.paintshop.domain;


import java.util.Set;

/**
 * Customer model. Contains the list of preferences (desired list of paints. each paint is unique)
 */
public interface Customer {

    /**
     * returns set of customers preferences
     * @return
     */
    public Set<Paint> paints();

    /**
     * Adds a paint to the customers preferences
     * @param p
     */
    void addPaint(Paint p);

    /**
     * Adds glossy paint with given color index
     * @param color index
     */
    default void addPaint(int color){
        addPaint(new Paint(color));
    }

    /**
     * Adds a paint with given color index and color option
     * @param color index
     * @param colorOption option
     */
    default void addPaint(int color, ColorOption colorOption){
        addPaint(new Paint(color, colorOption));
    }

    /**
     *
     * @return  actual size of the customer preferences set
     */
    default int size(){
        return paints().size();
    }

}
