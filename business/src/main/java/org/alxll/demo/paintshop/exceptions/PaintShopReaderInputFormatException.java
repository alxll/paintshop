package org.alxll.demo.paintshop.exceptions;


public class PaintShopReaderInputFormatException extends PaintShopBusinessException{
    public PaintShopReaderInputFormatException() {
    }

    public PaintShopReaderInputFormatException(Throwable cause) {
        super(cause);
    }

    public PaintShopReaderInputFormatException(String message) {
        super(message);
    }

    public PaintShopReaderInputFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public PaintShopReaderInputFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
