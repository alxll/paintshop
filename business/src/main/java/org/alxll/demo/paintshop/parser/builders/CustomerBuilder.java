package org.alxll.demo.paintshop.parser.builders;


import org.alxll.demo.paintshop.domain.Customer;

@FunctionalInterface
public interface CustomerBuilder {
    Customer customer(int maxColors);
}
