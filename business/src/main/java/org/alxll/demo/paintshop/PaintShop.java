package org.alxll.demo.paintshop;


import org.alxll.demo.paintshop.alg.BangkokPaintFactory;
import org.alxll.demo.paintshop.alg.PaintFactory;
import org.alxll.demo.paintshop.alg.ShanghaiPaintFactory;
import org.alxll.demo.paintshop.domain.Paint;
import org.alxll.demo.paintshop.domain.Survey;

import java.util.Set;

/**
 * Service class wrapping communication with PaintFactory
 *
 */
public class PaintShop {

    public PaintShop(){
    }

    public PaintFactory defaultPaintFactory(){
    	return bangkokPaintFactory();
    }

    public PaintFactory shanghaiPaintFactory(){
        return new ShanghaiPaintFactory();
    };

    public PaintFactory bangkokPaintFactory(){
        return new BangkokPaintFactory();
    }

    public Set<Paint> buildPaintRange(Survey testCase){
        return defaultPaintFactory().buildPaintRange(testCase);
    }


}
