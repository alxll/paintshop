package org.alxll.demo.paintshop.alg;


import org.alxll.demo.paintshop.domain.Paint;
import org.alxll.demo.paintshop.domain.Survey;

import java.util.SortedSet;

/**
 * PaintFactory implements the functionality to solve the problem
 * Like real factory produce the set of paints basing on the info provided
 */
public interface PaintFactory {
    /**
     * Finds optimized set of paints which satisfied the customers expectations from the given survey
     * @param survey
     * @return Set of paints
     */
    SortedSet<Paint> buildPaintRange(Survey survey);
}
