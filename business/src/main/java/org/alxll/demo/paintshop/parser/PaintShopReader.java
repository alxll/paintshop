package org.alxll.demo.paintshop.parser;

import org.alxll.demo.paintshop.domain.CustomerImpl;
import org.alxll.demo.paintshop.domain.Survey;
import org.alxll.demo.paintshop.domain.SurveyImpl;
import org.alxll.demo.paintshop.parser.builders.CustomerBuilder;
import org.alxll.demo.paintshop.parser.builders.SurveyBuilder;

import java.io.Closeable;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Interface declaring methods to read Surveys from any java.io.Reader
 */

// Alxll: it is better to move surveyBuilder(), customerBuilder() and surveyIterator() methods to an abstract class!
public interface PaintShopReader extends Closeable {
    boolean isOpen();
    Stream<Survey> read();

    /**
     * @return service class to build a Survey instance
     */
    default SurveyBuilder surveyBuilder(){
        return  SurveyImpl::new;
    }

    /**
     * @return service class to build a Customer instance
     */
    default CustomerBuilder customerBuilder(){
        return CustomerImpl::new;
    }

    /**
     * Builds Iterator<Survey> instance
     *
     * @param surveyNum number of expected surveys
     * @param surveySupplier instance of Supplier<Survey> encapsulating functionality of single Survey reading instantiation (e.g. reading from the java.io.Reader provided)
     * @return Iterator<Survey> instance
     */
    default Iterator<Survey> surveyIterator(int surveyNum, Supplier<Survey> surveySupplier){
        return new Iterator<Survey>() {
            Survey nextItem = null;
            int itemsProcessed = 0;

            @Override
            public boolean hasNext() {
                if(itemsProcessed < surveyNum && nextItem == null){
                    try {
                        nextItem = surveySupplier.get();
                    }
                    catch(NoSuchElementException e){
                    }
                }

                return nextItem!=null;


            }

            @Override
            public Survey next() {
                if(hasNext()){
                    Survey next = nextItem; nextItem = null;
                    itemsProcessed++;
                    return next;
                }

                throw new NoSuchElementException();
            }
        };

    }
}
