package org.alxll.demo.paintshop.domain;


import org.alxll.demo.paintshop.exceptions.PaintShopBusinessException;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @see org.alxll.demo.paintshop.domain.Customer
 */
public class CustomerImpl implements Customer {
    private int maxPaints;
    private final Set<Paint> paints = new HashSet<>();

    /**
     * Constructs new Customer with max number of paint allowed
     * @param maxPaints
     */
    public CustomerImpl(int maxPaints){
        if(maxPaints<=0){
            throw new IllegalArgumentException("MaxPaints must be positive!");
        }
        this.maxPaints = maxPaints;
    }

    @Override
    public Set<Paint> paints(){
        return Collections.unmodifiableSet(this.paints);
    }

    @Override
    public void addPaint(Paint p) {
        if(paints.size() == maxPaints){
            throw new IndexOutOfBoundsException("PaintPreferencesSet max capacity exceeded: "+maxPaints);
        }

        if(paints.stream().anyMatch(pp ->pp.colorIndex()==p.colorIndex() && !pp.colorOption().equals(p.colorOption()))){
            throw new PaintShopBusinessException("Duplicated color "+ p.colorIndex());
        }
        paints.add(p);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Customer)) return false;

        Customer that = (Customer) o;

        //Alxll: if not Set the order is important! Consider to sort collections in this case
        return paints().equals(that.paints());

    }
    @Override
    public int hashCode() {
        return paints.hashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("maxPaints", maxPaints)
                .append("paints", paints)
                .toString();
    }
}
