package org.alxll.demo.paintshop.exceptions;


public class PaintShopBusinessException extends RuntimeException{
    public PaintShopBusinessException() {
    }

    public PaintShopBusinessException(Throwable cause) {
        super(cause);
    }

    public PaintShopBusinessException(String message) {
        super(message);
    }

    public PaintShopBusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public PaintShopBusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
