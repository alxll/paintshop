package org.alxll.demo.paintshop.parser;


public class PaintShopReaderException extends RuntimeException{
    public PaintShopReaderException(String message){
        super(message);
    }

    public PaintShopReaderException(String message, Throwable t){
        super(message, t);
    }
}
