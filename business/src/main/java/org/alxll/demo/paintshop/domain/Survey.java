package org.alxll.demo.paintshop.domain;


import java.util.Collection;

/**
 * Survey is the definition of problem which has to be solved.
 * Contains the list of Customers, each of which contains their own list of preferences
 */
public interface Survey {
    /**
     *
     * @return actual number of customers in set
     */
    int customersNumber();

    /**
     *
     * @return max number of customers in set
     */
    int maxCustomers();

    /**
     *
     * @return total amount of colors available for the survey
     */
    int colorsTotal();

    /**
     * Anns given customer to the survey
     * @param s
     */
    void addCustomer(Customer s);

    /**
     * Returns collection of customers for given survey
     * @return
     */
    Collection<Customer> customers();
}
