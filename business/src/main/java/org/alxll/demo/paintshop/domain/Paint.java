package org.alxll.demo.paintshop.domain;



import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * Paint model. Each paint describer by color (color index) and type (ColorOption): glossy or matte
 */
public class Paint {
    private static final int MAX_COLOR_CODE = 2000;
    private final int color;
    private final ColorOption option;


    public Paint(int index) {
        this(index, ColorOption.glossy);
    }

    public Paint(int index, ColorOption option) {
        if(index <= 0 || index > MAX_COLOR_CODE){
            throw new IllegalArgumentException("Color code must be between 1 and "+MAX_COLOR_CODE);
        }

        this.color = index;
        this.option = option;
    }

    public int colorIndex() {
        return color;
    }


    public ColorOption colorOption() {
        return option;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Paint paint = (Paint) o;

        return color == paint.color && option == paint.option;

    }

    @Override
    public int hashCode() {
        int result = color;
        result = 31 * result + option.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("color", color)
                .append("option", option)
                .toString();
    }
}
