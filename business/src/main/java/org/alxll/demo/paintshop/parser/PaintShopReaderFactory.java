package org.alxll.demo.paintshop.parser;


import java.io.Reader;

public class PaintShopReaderFactory {

    public PaintShopReader paintShopReader(Reader r){
        return new PaintShopBufferedReaderImpl(r);
    }
}
