package org.alxll.demo.paintshop.parser.builders;


import org.alxll.demo.paintshop.domain.Survey;

@FunctionalInterface
public interface SurveyBuilder {
    Survey build(int maxColors, int customersNum);
}
