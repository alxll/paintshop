package org.alxll.demo.paintshop.domain;

import org.junit.Test;


public class SurveyImplTest {

    @Test(expected=IndexOutOfBoundsException.class)
    public void testNegativeColors(){
        new SurveyImpl(-1, 10);
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testZeroColors(){
        new SurveyImpl(0, 10);
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testNegativeCustomers(){
        new SurveyImpl(10, -1);
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testZeroCustomers(){
        new SurveyImpl(10, 0);
    }

    @Test(expected = NullPointerException.class)
    public void testAddCustomerNullNull() throws Exception {
        new SurveyImpl(10, 10).addCustomer(null);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddCustomerOutOfBounds() throws Exception {
        SurveyImpl s = new SurveyImpl(1, 1);
        s.addCustomer(new CustomerImpl(1));
        s.addCustomer(new CustomerImpl(1));
    }

}