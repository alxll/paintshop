package org.alxll.demo.paintshop.parser;

import org.alxll.demo.paintshop.domain.Survey;

import org.junit.Test;

import java.io.StringReader;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class PaintShopReaderScannerImplTest {


    @Test(expected = NullPointerException.class)
    public void testNulReader(){
        new PaintShopReaderScannerImpl(null);
    }

    @Test
    //Test reading valid input
    public void testValidInput(){
        String input = new StringBuilder()
                .append("2").append("\n")

                .append("5").append("\n")
                .append("3").append("\n")
                .append("1 1 1").append("\n")
                .append("2 1 0 2 0").append("\n")
                .append("1 5 0").append("\n")

                .append("1").append("\n")
                .append("2").append("\n")
                .append("1 1 0").append("\n")
                .append("1 1 1").append("\n")

                .toString();

        List<Survey> surveys = new PaintShopReaderScannerImpl(new StringReader(input)).read().collect(Collectors.toList());
        assertEquals(surveys.size(), 2);

        assertEquals(surveys.get(0).customersNumber(), 3);
        assertEquals(surveys.get(0).colorsTotal(), 5);

        assertEquals(surveys.get(1).customersNumber(), 2);
        assertEquals(surveys.get(1).colorsTotal(), 1);
    }

    @Test
    //Tests if the input contains more surveys then declared
    public void testMoreSurveys(){
        String input = new StringBuilder()
                .append("2").append("\n") // declared 2 surveys

                .append("5").append("\n")
                .append("3").append("\n")
                .append("1 1 1").append("\n")
                .append("2 1 0 2 0").append("\n")
                .append("1 5 0").append("\n")

                .append("1").append("\n")
                .append("2").append("\n")
                .append("1 1 0").append("\n")
                .append("1 1 1").append("\n")

                .append("2").append("\n")       //extra survey
                .append("2").append("\n")
                .append("2 1 0 2 1").append("\n")
                .append("2 1 1 2 0").append("\n")

                .toString();

        List<Survey> surveys = new PaintShopReaderScannerImpl(new StringReader(input)).read().collect(Collectors.toList());
        assertEquals(surveys.size(), 2);
    }

    @Test
    //Tests if the actual number of customers is less then declared
    public void testLessCustomers(){
        String input = new StringBuilder()
                .append("1").append("\n")

                .append("5").append("\n")
                .append("3").append("\n")           //declared 3
                .append("1 1 1").append("\n")
                .append("2 1 0 2 0").append("\n")   //actulal number is 2

                .toString();

        List<Survey> surveys = new PaintShopReaderScannerImpl(new StringReader(input)).read().collect(Collectors.toList());
        assertEquals(surveys.size(), 0);
    }


    @Test
    //Tests if the actual number of customers is more then declared
    public void testMoreCustomers(){
        String input = new StringBuilder()
                .append("1").append("\n")

                .append("5").append("\n")
                .append("2").append("\n")
                .append("1 1 1").append("\n")
                .append("2 1 0 2 0").append("\n")
                .append("2 1 1 2 1").append("\n")

                .toString();

        List<Survey> surveys = new PaintShopReaderScannerImpl(new StringReader(input)).read().collect(Collectors.toList());
        assertEquals(surveys.get(0).customers().size(), 2);
    }



    @Test
    //Tests if PaintPreferencesSet contains more paints then declared
    public void testExtraColors(){
        String input = new StringBuilder()
                .append("1").append("\n")

                .append("5").append("\n")
                .append("1").append("\n")
                .append("1 1 1 2 1").append("\n")
                .toString();

        List<Survey> surveys = new PaintShopReaderScannerImpl(new StringReader(input)).read().collect(Collectors.toList());

        assertEquals(surveys.get(0).customers().stream().findFirst().get().paints().size(), 1);
    }
}