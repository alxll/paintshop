package org.alxll.demo.paintshop.domain;

import org.alxll.demo.paintshop.exceptions.PaintShopBusinessException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CustomerImplTest {

    @Test(expected=IllegalArgumentException.class)
    public void testNegativeMaxPaints(){
        new CustomerImpl(-1);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testZeroMaxPaints(){
        new CustomerImpl(0);
    }


    @Test
    public void testAddPaint() throws Exception {
        CustomerImpl s = new CustomerImpl(10);
        s.addPaint(new Paint(1));

        assertEquals(s.paints().size(), s.paints().size(), 1);
    }

    @Test
    public void testAddPaintIndex() throws Exception {
        CustomerImpl s = new CustomerImpl(10);
        s.addPaint(1);

        assertEquals(s.paints().size(), s.paints().size(), 1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddPaintOutOfBounds() throws Exception {
        CustomerImpl s = new CustomerImpl(1);
        s.addPaint(new Paint(1));
        s.addPaint(new Paint(2));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddPaintIndexOutOfBounds() throws Exception {
        CustomerImpl s = new CustomerImpl(1);
        s.addPaint(1);
        s.addPaint(2);
    }


    @Test(expected = PaintShopBusinessException.class)
    public void testAddPaintDuplicatedColor() throws Exception {
        CustomerImpl s = new CustomerImpl(2);
        s.addPaint(new Paint(1));
        s.addPaint(new Paint(1, ColorOption.matte));
    }

    @Test(expected = PaintShopBusinessException.class)
    public void testAddPaintIndexDuplicatedColor() throws Exception {
        CustomerImpl s = new CustomerImpl(12);
        s.addPaint(1);
        s.addPaint(1, ColorOption.matte);
    }

    @Test
    public void testSize() throws Exception {
        CustomerImpl s = new CustomerImpl(10);
        s.addPaint(1);

        assertEquals(s.size(), s.size(), 2);
    }



    @Test
    public void testEquals() throws Exception {
        CustomerImpl s1 = new CustomerImpl(10);
        s1.addPaint(1);
        s1.addPaint(2);


        CustomerImpl s2 = new CustomerImpl(10);
        s2.addPaint(1);
        s2.addPaint(2);

        assertEquals(s1,s2);


    }
}