package org.alxll.demo.paintshop.alg;


import org.alxll.demo.paintshop.domain.*;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestCaseData {
    public static final Survey SURVEY_ONE;
    public static final Set<Paint> SURVEY_ONE_SOLUTION;

    public static final Survey SURVEY_TWO;
    public static final Set<Paint> SURVEY_TWO_SOLUTION;

    static {
        SURVEY_ONE = new SurveyImpl(1, 2);
        CustomerImpl customer = new CustomerImpl(1);
        customer.addPaint(1, ColorOption.glossy);
        SURVEY_ONE.addCustomer(customer);

        customer = new CustomerImpl(1);
        customer.addPaint(1, ColorOption.matte);
        SURVEY_ONE.addCustomer(customer);

        SURVEY_ONE_SOLUTION = Collections.emptySet();


        SURVEY_TWO = new SurveyImpl(5, 3);
        customer = new CustomerImpl(1);
        customer.addPaint(1, ColorOption.matte);
        SURVEY_TWO.addCustomer(customer);

        customer = new CustomerImpl(2);
        customer.addPaint(1, ColorOption.glossy);
        customer.addPaint(2, ColorOption.glossy);
        SURVEY_TWO.addCustomer(customer);

        customer = new CustomerImpl(1);
        customer.addPaint(5, ColorOption.matte);
        SURVEY_TWO.addCustomer(customer);

        SURVEY_TWO_SOLUTION = Stream.of(
                new Paint(1, ColorOption.matte),
                new Paint(2, ColorOption.glossy),
                new Paint(3, ColorOption.glossy),
                new Paint(4, ColorOption.glossy),
                new Paint(5, ColorOption.matte))
                .collect(Collectors.toSet());
    }

}
