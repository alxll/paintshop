package org.alxll.demo.paintshop.alg;

import org.alxll.demo.paintshop.domain.Paint;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;


public class BangkokPaintFactoryTest {

    @Test
    public void testProducePaints1() throws Exception {
        BangkokPaintFactory paintFactory = new BangkokPaintFactory();
        Set<Paint> result = paintFactory.buildPaintRange(TestCaseData.SURVEY_ONE);
        assertEquals(result, TestCaseData.SURVEY_ONE_SOLUTION);
    }

    @Test
    public void testProducePaints2() throws Exception {
        BangkokPaintFactory paintFactory = new BangkokPaintFactory();
        Set<Paint> result = paintFactory.buildPaintRange(TestCaseData.SURVEY_TWO);;
        assertEquals(result, TestCaseData.SURVEY_TWO_SOLUTION);
    }
}