package org.alxll.demo.paintshop.domain;


import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PaintTest {

    @Test
    public void testDefaultColorOption(){
        assertEquals(new Paint(1).colorOption(), ColorOption.glossy);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeColorIndex(){
        new Paint(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroColorIndex(){
        new Paint(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooBigColorIndex(){
        new Paint(2001);
    }

    @Test
    public void testEquals(){
        assertEquals(new Paint(1), new Paint(1));
        assertEquals(new Paint(2, ColorOption.glossy), new Paint(2));
        assertEquals(new Paint(3, ColorOption.glossy), new Paint(3, ColorOption.glossy));
        assertEquals(new Paint(4, ColorOption.matte), new Paint(4, ColorOption.matte));

        assertNotEquals(new Paint(5), new Paint(6));
        assertNotEquals(new Paint(7), new Paint(7, ColorOption.matte));
    }


}