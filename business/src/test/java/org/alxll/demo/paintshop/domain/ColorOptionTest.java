package org.alxll.demo.paintshop.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ColorOptionTest {

    @Test
    public void testValueOfGlossy() throws Exception {
        assertEquals(ColorOption.valueOf(0), ColorOption.glossy);
    }

    @Test
    public void testValueOfMatte() throws Exception {
        assertEquals(ColorOption.valueOf(1), ColorOption.matte);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void invalidOption(){
        ColorOption.valueOf(999);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void invalidOptionNegative(){
        ColorOption.valueOf(-1);
    }
}