package org.alxll.demo.paintshop.commands;


import org.alxll.demo.paintshop.PantShopAppTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.shell.core.CommandResult;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class PaintShopCommandsTest extends PantShopAppTest {

    @Before
    public void before() throws IOException {
        Files.deleteIfExists(Paths.get("output.txt"));
    }

    @After
    public void after(){
    }

    @Test
    // Alxll: command is too complex. Required be move functionality to external class and testing independently. Command testing has to test isSuccess() only
    public void testProcessValidFile(){
      CommandResult cr = getShell().executeCommand("process test-survey.txt");

        assertTrue(cr.isSuccess());
        assertTrue(cr.getResult() instanceof List);
//        assertEquals(cr.getResult(), Arrays.asList("Case #1: 1 0 0 0 0","Case #2: IMPOSSIBLE"));
    }

    @Test
    public void testProcessValidFileWithOutput(){
        CommandResult cr = getShell().executeCommand("process test-survey.txt --o output.txt");

        assertTrue(Files.exists(Paths.get("output.txt")));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testProcessNonExistedFile(){
        CommandResult cr = getShell().executeCommand("process bla-bla-bla.txt");

        assertNotNull(cr.getResult());
        assertTrue(((List<String>)cr.getResult()).get(0).contains("Error"));
    }

}