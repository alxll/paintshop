package org.alxll.demo.paintshop;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.shell.Bootstrap;

import java.io.IOException;

/**
 * Main class to run console application
 */
@SpringBootApplication
public class PantShopApp{

    private static final Logger log = LoggerFactory.getLogger("PaintShop");

    public static void main(String args[]) throws IOException {
        Bootstrap.main(args);
    }


}