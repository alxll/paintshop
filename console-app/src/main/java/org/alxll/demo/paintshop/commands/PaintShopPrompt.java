package org.alxll.demo.paintshop.commands;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.PromptProvider;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
/**
 * @see PromptProvider
 */
public class PaintShopPrompt implements PromptProvider {
    @Override
    public String getPrompt() {
        return "#> ";
    }


    @Override
    public String getProviderName() {
        return "Paint Shop Prompt";
    }

}
