package org.alxll.demo.paintshop.commands;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.support.DefaultHistoryFileNameProvider;
import org.springframework.stereotype.Component;


@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
/**
 * @see org.springframework.shell.plugin.HistoryFileNameProvider
 */
public class PaintShopHistoryFile extends DefaultHistoryFileNameProvider {

    public String getHistoryFileName() {
        return "history.log";
    }

    @Override
    public String getProviderName() {
        return "Paint Shop history file name provider";
    }

}