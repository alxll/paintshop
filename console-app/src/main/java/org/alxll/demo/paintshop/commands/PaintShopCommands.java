package org.alxll.demo.paintshop.commands;

import org.alxll.demo.paintshop.PaintShop;
import org.alxll.demo.paintshop.parser.PaintShopReader;
import org.alxll.demo.paintshop.parser.PaintShopReaderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @see org.springframework.shell.core.CommandMarker
 */
@Component
public class PaintShopCommands implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger("PaintShop");
    private static final Logger console = LoggerFactory.getLogger("console-output");

    @Autowired
    private PaintShopReaderFactory paintShopReaderFactory;

    @CliAvailabilityIndicator({"process"})
    public boolean isProcessFileAvailable() {
        return true;
    }


    /**
     * Defines the command to process input file
     * Possible options are:
     *      #> process your-file-name
     *      or
     *      #> process your-file-name --o output-file+name
     *
     *      The result of the processing will be printed on screen
     *      but if output file name provided, results will be placed in output file also
     *
     * @return list of strings in format defined in the project requirements or list of errors
     */
    //Alxll: return is not required but helpful for testing but unfortunately it duplicates output :(
    @CliCommand(value = "process", help = "Process file")
    public List<String> processFile(@CliOption(key = { "" }, mandatory = true, help = "Input file name to process") final String input,
                            @CliOption(key = { "output", "o" }, mandatory = false, help = "Output file name. If not provided the console output only will be used") final String output)  throws IOException{

        Path pth = Paths.get(input);
        if(Files.notExists(pth)){
            String err = "Input file "+input+" does not exists";
            log.error(err);
            return Arrays.asList("Error: "+ err);
        }

        PaintShop ps = new PaintShop();

        try(PaintShopReader r = paintShopReaderFactory.paintShopReader(Files.newBufferedReader(pth));
            PrintWriter o = this.outputWriter(output)){
            AtomicInteger indx = new AtomicInteger(1);
            console.info("\nProcessing " + input+":");
            List<String> result =  r.read()
                    .map(ps::buildPaintRange)
                    .map(paints -> paints.stream()
                            .map(p -> new StringBuilder().append(p.colorOption().ordinal()))
                            .collect(Collectors.reducing((s1, s2) -> s1.append(" ").append(s2)))
                            .orElse(new StringBuilder("IMPOSSIBLE"))
                            .toString())
                    .map(cs -> "Case #"+indx.getAndIncrement()+": "+cs)
                    .peek(console::info)
                    .peek(o::println)
                    .collect(Collectors.toList());

            console.info("---------------------------------------");
            console.info("Complete: "+result.size()+" test cases processed");
            return result;
        }


    }

    private PrintWriter outputWriter(String fileName) throws IOException {
        if(Objects.isNull(fileName)){
            return new PrintWriter(new OutputStream(){
                @Override
                public void write(int b) throws IOException {
                }
            });
            }

            return new PrintWriter(Files.newBufferedWriter(Paths.get(fileName), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING));

        }

    }

