package org.alxll.demo.paintshop.cfg;

import org.alxll.demo.paintshop.parser.PaintShopReaderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PaintShopAppConfig {

    @Bean
    public PaintShopReaderFactory paintShopReaderFactory(){
        return new PaintShopReaderFactory();
    }
}
